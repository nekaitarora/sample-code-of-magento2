<?php

/**
* @category  Vivomo
*
* @author    Priyatosh <priyatosh.ojha@daffodilsw.com>
*/

namespace Vivomo\Campaign\Controller\Campaign;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Vivomo\Campaign\Model\Campaign;

/**
* Vivomo Campaign ShowCampaign controller.
*/
class CampaignDetail extends Action
{
   /**
    * @var PageFactory
    */
   protected $_resultPageFactory;

   /**
    * @param Context     $context
    * @param PageFactory $resultPageFactory
    */
   public function __construct(
       Context $context,
       PageFactory $resultPageFactory
   ) {
       $this->_resultPageFactory = $resultPageFactory;
       parent::__construct($context);
   }

   /**
    * Show product lists according to campaign id.
    *
    * @return \Magento\Framework\View\Result\Page
    */
   public function execute()
   {
       $campaignData = $this->getRequest()->getParams();
       $campaignId = '';
       $campaignTitle = '';
       $campaignStatus = '';
       $visibleStatus = Campaign::STATUS_ENABLED;
       foreach ($campaignData as $key => $value) {
           $campaignId = $value;
           break;
       }
       if ($campaignId) {
           $campaignModel = $this->_objectManager->get(Campaign::class)->load($campaignId);
           $campaignTitle = $campaignModel->getCampaignTitle();
           $campaignStatus = (int)$campaignModel->getStatus();
       }
       if ($campaignStatus !== $visibleStatus) {

           return $this->resultRedirectFactory->create()->setPath(
               '/',['_secure' => $this->getRequest()->isSecure()]
           );
       }
       if (!$campaignTitle) {
           $campaignTitle = 'Campaign Product Page';
       }
       $resultPage = $this->_resultPageFactory->create();
       $resultPage->getConfig()->getTitle()->set(__($campaignTitle));

       return $resultPage;
   }
}
