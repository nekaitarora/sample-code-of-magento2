<?php

/**
 * @category  Vivomo
 *
 * @author    Priyatosh <priyatosh.ojha@daffodilsw.com>
 */

namespace Vivomo\Campaign\Helper;

use Magento\Backend\Model\UrlInterface;
use Magento\Catalog\Model\CategoryFactory;
use Magento\Catalog\Model\ProductFactory;
use Magento\Catalog\Model\ProductRepository;
use Magento\Directory\Model\Country;
use Magento\Catalog\Model\Product;
use Magento\Customer\Model\Address;
use Magento\Checkout\Model\Cart;
use Magento\Customer\Model\Session;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\UrlInterface as MediaUrl;
use Magento\Framework\Pricing\Helper\Data as FormatPrice;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Store\Model\ScopeInterface;
use Webkul\Marketplace\Model\Product as WebkulProduct;
use Vivomo\Campaign\Model\Campaign as CampaignObj;
use Vivomo\Campaign\Model\Orders as ShippingOrders;
use Webkul\Marketplace\Helper\Data as WebkulHelper;

/**
 * Class Data.
 */
class Data extends AbstractHelper
{
    /**
     * Campaign's Discount Type.
     */
    const DISCOUNT_TYPE_FIXED = 1;
    const DISCOUNT_TYPE_PERCENTAGE = 2;
    const DISCOUNT_TYPE_NODISCOUNT = 3;

    /**
     * Item Product's Type.
     */
    const ITEM_PRODUCT_TYPE = 'configurable';

    /**
     * Campaign's Discount Text.
     */
    const DISCOUNT_TYPE_FIXED_TEXT = 'Flat';
    const DISCOUNT_TYPE_PERCENTAGE_TEXT = 'Percent';
    const DISCOUNT_TYPE_NODISCOUNT_TEXT = 'No Discount';

    /**
     * Per Country Default Price Path.
     */
    const MPPERCOUNTRY_DEFAULT_PRICE_PATH = 'carriers/mppercountry/default_amount';

    /**
     * DEFAULT SHIPPING PRICE.
     */
    const DEFAULT_SHIPPING_PRICE = 0;

    /**
     * @var UrlInterface
     */
    protected $_backendUrl;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * Core store config.
     *
     * @var ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var ProductFactory
     */
    protected $_productFactory;

    /**
     * @var CategoryFactory
     */
    protected $_categoryFactory;

    /**
     * @var ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     * @var WebkulHelper
     */
    protected $_webkulHelper;

    /**
     * @var CheckoutSession
     */
    protected $_checkoutSession;

    /**
     * @var ProductRepository
     */
    protected $_productRepository;

    /**
     * Data constructor.
     *
     * @param Context                $context
     * @param UrlInterface           $backendUrl
     * @param StoreManagerInterface  $storeManager
     * @param ObjectManagerInterface $objectManager
     * @param ProductFactory         $productFactory
     * @param CategoryFactory        $categoryFactory
     * @param WebkulHelper           $webkulHelper
     * @param CheckoutSession        $checkoutSession
     * @param ProductRepository      $productRepository
     */
    public function __construct(
        Context $context,
        UrlInterface $backendUrl,
        StoreManagerInterface $storeManager,
        ObjectManagerInterface $objectManager,
        ProductFactory $productFactory,
        CategoryFactory $categoryFactory,
        WebkulHelper $webkulHelper,
        CheckoutSession $checkoutSession,
        ProductRepository $productRepository
    ) {
        parent::__construct($context);
        $this->_backendUrl = $backendUrl;
        $this->storeManager = $storeManager;
        $this->_scopeConfig = $context->getScopeConfig();
        $this->_objectManager = $objectManager;
        $this->_productFactory = $productFactory;
        $this->_categoryFactory = $categoryFactory;
        $this->_webkulHelper = $webkulHelper;
        $this->_checkoutSession = $checkoutSession;
        $this->_productRepository = $productRepository;
    }

    /**
     * @return string
     */
    public function getProductsGridUrl()
    {
        return $this->_backendUrl->getUrl('vivomo_campaign/campaign/products', ['_current' => true]);
    }

    /**
     * @return string
     */
    public function getMediaUrl()
    {
        return $this->storeManager->getStore()->getBaseUrl(
            MediaUrl::URL_TYPE_MEDIA
        );
    }

    /**
     * Get Campaign Product Price.
     *
     * @param float $productPrice
     * @param int   $discountType
     * @param int   $discountPrice
     *
     * @return string
     */
    public function getPrice($productPrice, $discountType, $discountPrice)
    {
        $fixedType = self::DISCOUNT_TYPE_FIXED;
        $percentType = self::DISCOUNT_TYPE_PERCENTAGE;
        if ($discountType === $fixedType) { //check if discount type is fixed.
            $price = $discountPrice;
        } elseif ($discountType === $percentType) { //check if discount type is percentage.
            $price = ($discountPrice * $productPrice) / 100;
        } else { //check if no discount type applied.
            $price = $productPrice;
        }
        $priceHelper = $this->_objectManager->create(FormatPrice::class);
        $formattedPrice = $priceHelper->currency($price, true, false);

        return $formattedPrice;
    }

    /**
     * Get Campaign Product Price without price format.
     *
     * @param float $productPrice
     * @param int   $discountType
     * @param int   $discountPrice
     *
     * @return float
     */
    public function getPriceWithoutFormat($productPrice, $discountType, $discountPrice)
    {
        $fixedType = self::DISCOUNT_TYPE_FIXED;
        $percentType = self::DISCOUNT_TYPE_PERCENTAGE;
        if ($discountType === $fixedType) { //check if discount type is fixed.
            $price = $discountPrice;
        } elseif ($discountType === $percentType) { //check if discount type is percentage.
            $price = ($discountPrice * $productPrice) / 100;
        } else { //check if no discount type applied.
            $price = $productPrice;
        }

        return $price;
    }

    /**
     * Change price in price format.
     *
     * @param float $finalPrice
     *
     * @return string
     */
    public function formatPrice($finalPrice)
    {
        $priceHelper = $this->_objectManager->create(FormatPrice::class);
        $formattedPrice = $priceHelper->currency($finalPrice, true, false);

        return $formattedPrice;
    }

    /**
     * Get Shipping Price of product.
     *
     * @param int $productId
     *
     * @return float
     */
    public function getShippingPrice($productId)
    {
        $shippingData = $this->_objectManager->create(Product::class)->load($productId)->getMpShippingCountryCharge();
        $shippingId = $this->_objectManager->create(Session::class)->getCustomer()->getDefaultShipping();
        $defaultPricePath = self::MPPERCOUNTRY_DEFAULT_PRICE_PATH;
        $shipPrice = $this->_scopeConfig->getValue($defaultPricePath, ScopeInterface::SCOPE_STORE, $this->storeManager->getStore());
        if ($shippingId) {
            $addressData = $this->_objectManager->create(Address::class)->load($shippingId)->getData();
            $countryId = $addressData['country_id'];
            $shippingBreak = explode('/', $shippingData);
            $defaultShippingPrice = self::DEFAULT_SHIPPING_PRICE;
            foreach ($shippingBreak as $ship) {
                $shipPriceData = explode(',', $ship);
                if (is_array($shipPriceData)) {
                    $countryShipId = isset($shipPriceData[0]) ? $shipPriceData[0] : null;
                    if ($countryShipId === $countryId) {
                        $shipPrice = isset($shipPriceData[1]) ? $shipPriceData[1] : $defaultShippingPrice;
                        break; //if match the exit from loop.
                    }
                }
            }
        }

        return $shipPrice;
    }

    /**
     * Get Cart price with shipping price.
     *
     * @return Cart[]
     */
    public function getCurrentCartItems()
    {
        $cart = $this->_objectManager->get(Cart::class);
        $quote = $cart->getQuote();
        $itemsCollection = $quote->getItemsCollection();
        // retrieve quote items array
        $items = $quote->getAllItems();
        $shippingPrice = self::DEFAULT_SHIPPING_PRICE;
        $configType = self::ITEM_PRODUCT_TYPE;
        foreach ($items as $item) {
            if ($item->getParentItemId()) {
                continue;
            }
            if ($item->getProductType() == $configType) {
                $simpleProduct = $item->getOptionByCode('simple_product')->getProduct();
                $productId = $simpleProduct->getId();
            } else {
                $productId = $item->getProductId();
            }
            $qty = $item->getQty();
            $shippingTempPrice = $this->getShippingPrice($productId) * $qty;
            $shippingPrice = $shippingPrice + $shippingTempPrice;
        }
        $cartData = [];
        $shippingTax = $shippingPrice;
        $totals = $quote->getTotals();
        $subTotal = $totals['subtotal']->getValueInclTax();
        $taxAmount = $totals['tax']->getValue();
        $cartData['subTotal'] = $subTotal;
        $cartData['taxAmount'] = $taxAmount;
        $cartData['shipping'] = $shippingPrice;
        $cartData['shippingTax'] = $shippingTax;

        return $cartData;
    }

    /**
     * Get seller information and shipping.
     *
     * @param int $quoteId
     *
     * @return Cart[]
     */
    public function getSellerWithShipping($quoteId)
    {
        $cart = $this->_objectManager->get(Cart::class);
        $quote = $cart->getQuote();
        $items = $quote->getAllItems();
        $defaultPricePath = self::MPPERCOUNTRY_DEFAULT_PRICE_PATH;
        $shippingPrice = $this->_scopeConfig->getValue($defaultPricePath, ScopeInterface::SCOPE_STORE, $this->storeManager->getStore());
        $sellerName = '';
        $sellerShopUrl = '';
        foreach ($items as $item) {
            if ($item->getParentItemId()) {
                continue;
            }
            $itemId = $item->getId();
            if ($itemId === $quoteId) {
                $productId = $item->getProductId();
                $qty = $item->getQty();
                $shippingPrice = $this->getShippingPrice($productId) * $qty;
                $productCollection = $this->_objectManager->create(WebkulProduct::class)->getCollection()->addFieldToFilter('mageproduct_id', $productId);
                foreach ($productCollection as $product) {
                    $sellerId = $product->getSellerId();
                    $sellerObj = $this->_webkulHelper->getSellerDataBySellerId($sellerId);
                    foreach ($sellerObj as $sellerData) {
                        $sellerName = isset($sellerData['shop_title']) ? $sellerData['shop_title'] : '';
                        $sellerShopUrl = isset($sellerData['shop_url']) ? $sellerData['shop_url'] : '';
                        if (!$sellerName) {
                            $sellerName = $sellerData->getShopUrl();
                        }
                    }
                }
                break;
            }
        }
        $cartData = [];
        $sellerUrl = $this->_webkulHelper->getRewriteUrl('marketplace/seller/profile/shop/' . $sellerShopUrl);
        $shippingTaxPrice = $shippingPrice; //shipping tax calculation
        $shippingPrice = $shippingPrice + $shippingTaxPrice;
        $shippingPrice = $this->formatPrice($shippingPrice);
        $cartData['seller'] = $sellerName;
        $cartData['shop_url'] = $sellerUrl;
        $cartData['shipping'] = $shippingPrice;

        return $cartData;
    }

    /**
     * @param int $quoteId
     *
     * @return array
     */
    public function getSellerDataFromCartQuoteId($quoteId)
    {
        $items = $this->_checkoutSession->getQuote()->getAllItems();
        $sellerName = '';
        $sellerShopUrl = '';
        if (!empty($items)) {
            foreach ($items as $item) {
                if ($item->getParentItemId()) {
                    continue;
                }
                $itemId = $item->getId();
                if ($itemId === $quoteId) {
                    $productId = $item->getProductId();
                    $productCollection = $this->_objectManager
                        ->create(WebkulProduct::class)
                        ->getCollection()
                        ->addFieldToFilter('mageproduct_id', $productId);
                    foreach ($productCollection as $product) {
                        $sellerId = $product->getSellerId();
                        $sellerObj = $this->_webkulHelper->getSellerDataBySellerId($sellerId);
                        foreach ($sellerObj as $sellerData) {
                            $sellerShopUrl = isset($sellerData['shop_url']) ? $sellerData['shop_url'] : '';
                            $sellerName = isset($sellerData['shop_title']) && !empty($sellerData['shop_title']) ? $sellerData['shop_title'] : $sellerShopUrl;
                            if ($sellerName !== '') {
                                break 3; // exit all loops if seller name is found
                            }
                        }
                    }
                }
            }
        }
        $cartData = [];
        $sellerUrl = $this->_webkulHelper->getRewriteUrl('marketplace/seller/profile/shop/' . $sellerShopUrl);
        $cartData['seller'] = $sellerName;
        $cartData['shop_url'] = $sellerUrl;

        return $cartData;
    }

    /**
     * Get no discount type.
     *
     * @return int
     */
    public function getNoDiscountType()
    {
        $noDiscount = self::DISCOUNT_TYPE_NODISCOUNT;

        return $noDiscount;
    }

    /**
     * Get all discount types.
     *
     * @return array
     */
    public function getDiscountTypes()
    {
        $noDiscount = self::DISCOUNT_TYPE_NODISCOUNT;
        $fixedType = self::DISCOUNT_TYPE_FIXED;
        $percentType = self::DISCOUNT_TYPE_PERCENTAGE;
        $noDiscountText = self::DISCOUNT_TYPE_NODISCOUNT_TEXT;
        $fixedTypeText = self::DISCOUNT_TYPE_FIXED_TEXT;
        $percentTypeText = self::DISCOUNT_TYPE_PERCENTAGE_TEXT;
        $discountTypes = [];
        $discountTypes[$fixedType] = $fixedTypeText;
        $discountTypes[$percentType] = $percentTypeText;
        $discountTypes[$noDiscount] = $noDiscountText;

        return $discountTypes;
    }

    /**
     * get manufacturer sku by product id.
     *
     * @param int $productId
     *
     * @return string
     */
    public function getManufacturerSkuByProductId($productId)
    {
        $manufacturerSku = '';
        $product = $this->_objectManager->create(Product::class)->load($productId);
        if ($product) {
            $manufacturerSku = $product->getSkuManufacturer();
        }

        return $manufacturerSku;
    }

    /**
     * get manufacturer sku by product sku.
     *
     * @param string $productSku
     *
     * @return string
     */
    public function getManufacturerSkuByProductSku($productSku)
    {
        $manufacturerSku = '';
        $product = $this->_productRepository->get($productSku);
        if ($product) {
            $manufacturerSku = $product->getSkuManufacturer();
        }

        return $manufacturerSku;
    }

    /**
     * Get coming soon campaign Collection.
     *
     * @return Collection
     */
    public function getComingSoonCampaignCollection()
    {
        $currentDateTime = date('Y-m-d H:i:s');
        $comingSoonCampaignModel = $this->_objectManager->get(CampaignObj::class)->getCollection()
            ->addFieldToFilter('start_time', ['gt' => $currentDateTime])
            ->addFieldToFilter('show_on_homepage', 1);

        return $comingSoonCampaignModel;
    }

    /**
     * get shipping price by order item id.
     *
     * @param int $orderId
     * @param int $itemId
     *
     * @return float
     */
    public function getShippingPriceByOrderItemId($orderId, $itemId)
    {
        $OrdersObj = $this->_objectManager->create(ShippingOrders::class)->getCollection()->addFieldToFilter('item_id', $itemId)->addFieldToFilter('order_id', $orderId);
        $shippingPrice = 0;
        foreach ($OrdersObj as $key) {
            $shippingPrice = $key->getShippingPrice();
        }

        return $shippingPrice;
    }

    /**
     * get shipping prices by product id.
     *
     * @param int $productId
     *
     * @return string
     */
    public function getShippingPricesWithCountryByProductId($productId)
    {
        $productModel = $this->_objectManager->create(Product::class)->load($productId);
        $shipPrice = $productModel->getMpShippingCountryCharge();

        return $shipPrice;
    }

    /**
     * Get Country Name.
     *
     * @param null|int $countryCode
     *
     * @return string
     */
    public function getCountryName($countryCode = null)
    {
        $countryName = '';
        if ($countryCode) {
            $countryName = $this->_objectManager->create(Country::class)->loadByCode($countryCode)->getName();
        }

        return $countryName;
    }

    /**
     * Get Default Shipping.
     *
     * @return float
     */
    public function getDefaultShipping()
    {
        $defaultPricePath = self::MPPERCOUNTRY_DEFAULT_PRICE_PATH;
        $shipPrice = $this->_scopeConfig->getValue($defaultPricePath, ScopeInterface::SCOPE_STORE, $this->storeManager->getStore());
        $shipPrice = $this->formatPrice($shipPrice);

        return $shipPrice;
    }

    /**
     * get shipping prices by product id.
     *
     * @param int    $productId
     * @param string $countryId
     *
     * @return float
     */
    public function getShippingPriceByProductIdCountryId($productId, $countryId)
    {
        $shippingData = $this->_objectManager->create(Product::class)->load($productId)->getMpShippingCountryCharge();
        $defaultPricePath = self::MPPERCOUNTRY_DEFAULT_PRICE_PATH;
        $shipPrice = $this->_scopeConfig->getValue($defaultPricePath, ScopeInterface::SCOPE_STORE, $this->storeManager->getStore());
        if ($countryId) {
            $shippingBreak = explode('/', $shippingData);
            $defaultShippingPrice = self::DEFAULT_SHIPPING_PRICE;
            foreach ($shippingBreak as $ship) {
                $shipPriceData = explode(',', $ship);
                if (is_array($shipPriceData)) {
                    $countryShipId = isset($shipPriceData[0]) ? $shipPriceData[0] : null;
                    if ($countryShipId === $countryId) {
                        $shipPrice = isset($shipPriceData[1]) ? $shipPriceData[1] : $defaultShippingPrice;
                        break; //if match the exit from loop.
                    }
                }
            }
        }

        return $shipPrice;
    }

    /**
     * @param int $productId
     *
     * @return string
     */
    public function getProductCategoryName($productId)
    {
        $categoryName = '';
        $product = $this->_productFactory->create()->load($productId);
        if ($product) {
            $categoryIds = $product->getCategoryIds();
            if (!empty($categoryIds)) {
                $lastCategoryId = (int) array_pop($categoryIds);
                $category = $this->_categoryFactory->create()->load($lastCategoryId);
                if ($category) {
                    $categoryName = $category->getName();
                }
            }
        }

        return $categoryName;
    }
}
