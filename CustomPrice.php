<?php
/**
 * @category  Vivomo
 *
 * @author    Priyatosh <priyatosh.ojha@daffodilsw.com>
 */

namespace Vivomo\Campaign\Observer;

use Magento\Catalog\Model\ProductRepository;
use Magento\Checkout\Model\Cart;
use Magento\Framework\Data\Form\FormKey;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\ObjectManagerInterface;
use Vivomo\Campaign\Model\Campaign;
use Vivomo\Campaign\Model\Productlist;
use Vivomo\Campaign\Helper\Data as CampaignHelper;
use Magento\Framework\Stdlib\DateTime\DateTime;

/**
 * Class CustomPrice
 * Create to custom price when add to cart for campaign.
 */
class CustomPrice implements ObserverInterface
{
    /**
     * @var ProductRepository
     */
    protected $_productRepository;

    /**
     * @var Cart
     */
    protected $_cart;

    /**
     * @var FormKey
     */
    protected $formKey;

    /**
     * @var ObjectManagerInterface|null
     */
    protected $_objectManager = null;

    /**
     * @var CampaignHelper
     */
    protected $_helper;

    /**
     * @var DateTime class
     */
    protected $_dateTime;

    /**
     * CustomPrice constructor.
     *
     * @param ProductRepository      $productRepository
     * @param Cart                   $cart
     * @param ObjectManagerInterface $objectManager
     * @param FormKey                $formKey
     * @param DateTime               $dateTime
     * @param CampaignHelper         $campaignHelper
     */
    public function __construct(
        ProductRepository $productRepository,
        Cart $cart,
        ObjectManagerInterface $objectManager,
        FormKey $formKey,
        DateTime $dateTime,
        CampaignHelper $campaignHelper
    ) {
        $this->_productRepository = $productRepository;
        $this->_cart = $cart;
        $this->_objectManager = $objectManager;
        $this->formKey = $formKey;
        $this->_dateTime = $dateTime;
        $this->_helper = $campaignHelper;
    }

    /**
     * {@inheritdoc}
     */
    public function execute(Observer $observer)
    {
        $item = $observer->getEvent()->getData('quote_item');
        $product = $observer->getEvent()->getData('product');
        $productId = $product->getId();
        $productPrice = $product->getFinalPrice();
        $model = $this->_objectManager->create(Productlist::class)->getCollection()->addFieldToFilter('product_id', $productId);
        if (count($model)) {
            $campaignId = '';
            foreach ($model as $ke) {
                $campaignId = $ke->getCampaignId();
            }
            $campaignModel = $this->_objectManager->create(Campaign::class)->load($campaignId);
            $startTime = strtotime($campaignModel->getStartTime());
            $endTime = strtotime($campaignModel->getEndTime());
            $currentTime = $this->_dateTime->gmtTimestamp();
            if ($currentTime > $startTime && $currentTime < $endTime) { //check campaign is avaialble between campaign date.
                // Set the custom price.
                $item->setCustomPrice($productPrice);
                $item->setOriginalCustomPrice($productPrice);
                // Enable super mode on the product.
                $item->getProduct()->setIsSuperMode(true);
            }
        }

        return $this;
    }
}
